import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PlanetListComponent} from './component/planet-list/planet-list.component';
import {PlanetNewComponent} from './component/planet-new/planet-new.component';

const routes: Routes = [
  {
    path: 'planets',
    component: PlanetListComponent
    // canActivate: [AuthGuard] // TODO: reactivate
  },
  {
    path: 'planets/new',
    component: PlanetNewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PlanetRoutingModule { }
