export interface Planet {
  id: number;
  name: string;
  size: number;
  color: string;
  solarSystemId: number;
}
