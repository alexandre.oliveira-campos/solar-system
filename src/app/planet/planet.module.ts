import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PlanetListComponent} from './component/planet-list/planet-list.component';
import {PlanetNewComponent} from './component/planet-new/planet-new.component';
import {PlanetRoutingModule} from './planet-routing.module';
import {FormsModule} from '@angular/forms';
import { PlanetComponent } from './component/planet/planet.component';



@NgModule({
  declarations: [
    PlanetListComponent,
    PlanetNewComponent,
    PlanetComponent
  ],
  imports: [
    CommonModule,
    PlanetRoutingModule,
    FormsModule
  ]
})
export class PlanetModule { }
