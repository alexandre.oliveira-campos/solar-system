import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Planet} from '../model/planet';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {

  constructor(private http: HttpClient) { }

  url = 'https://localhost:5001';

  getPlanets(): Observable<Array<Planet>> {
    return this.http.get<[Planet]>(this.url + '/planets');
  }

  addPlanet(planet: Planet): Observable<Array<Planet>> {
    return this.http.post<[Planet]>(this.url + '/planets/new', planet);
  }

  editPlanet(id: number, planet: Planet): Observable<Array<Planet>> {
    return this.http.post<[Planet]>(this.url + '/planets/' + id + '/edit', planet);
  }

  deletePlanet(planet: Planet): Observable<Array<Planet>> {
    return this.http.delete<[Planet]>(this.url + '/planets/' + planet.id + '/delete');
  }
}
