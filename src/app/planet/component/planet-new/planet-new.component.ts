import { Component, OnInit } from '@angular/core';
import {Planet} from '../../model/planet';
import {PlanetService} from '../../service/planet.service';

@Component({
  selector: 'app-planet-new',
  templateUrl: './planet-new.component.html',
  styleUrls: ['./planet-new.component.scss']
})
export class PlanetNewComponent implements OnInit {

  constructor(private planetService: PlanetService) { }

  planet = {} as Planet;

  planets: Planet[] = [];

  ngOnInit(): void {
    this.getPlanets();
  }

  getPlanets(): void {
    this.planetService.getPlanets().subscribe(
      data =>
      {
        if (data) {
          this.planets = data;
        }
      },
      error => {}
    );
  }

  addPlanet(): void {
    this.planet.id = Math.max(0, ...this.planets.map(p => p.id)) + 1;
    this.planetService.addPlanet(this.planet).subscribe(() => this.getPlanets());
  }

}
