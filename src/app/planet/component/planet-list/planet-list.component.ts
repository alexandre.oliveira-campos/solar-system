import { Component, OnInit } from '@angular/core';
import {PlanetService} from '../../service/planet.service';
import {Planet} from '../../model/planet';

@Component({
  selector: 'app-planet-list',
  templateUrl: './planet-list.component.html',
  styleUrls: ['./planet-list.component.scss']
})
export class PlanetListComponent implements OnInit {

  constructor(private planetService: PlanetService) { }

  planets: Planet[];

  ngOnInit(): void {
    this.getPlanets();
  }

  getPlanets(): void {
    this.planetService.getPlanets().subscribe(
      data =>
      {
        if (data) {
          this.planets = data;
        }
      },
      error => {}
    );
  }

  deletePlanet(planet: Planet): void {
    this.planetService.deletePlanet(planet).subscribe(() => this.getPlanets());
  }
}
