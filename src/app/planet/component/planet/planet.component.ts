import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Planet} from '../../model/planet';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {

  constructor() { }

  @Input() planet: Planet;
  @Output() readonly planetSelected = new EventEmitter<Planet>();

  ngOnInit(): void {
  }

  delete(): void {
    this.planetSelected.emit(this.planet);
  }

}
