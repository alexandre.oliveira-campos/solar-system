import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SolarSystemListComponent} from './component/solar-system-list/solar-system-list.component';

const routes: Routes = [
  {
    path: 'solar-systems',
    component: SolarSystemListComponent
    // canActivate: [AuthGuard] // TODO: reactivate
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SolarSystemRoutingModule { }
