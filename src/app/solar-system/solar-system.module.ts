import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SolarSystemRoutingModule} from './solar-system-routing.module';
import {SolarSystemListComponent} from './component/solar-system-list/solar-system-list.component';
import {SolarSystemNewComponent} from './component/solar-system-new/solar-system-new.component';



@NgModule({
  declarations: [
    SolarSystemListComponent,
    SolarSystemNewComponent
  ],
  imports: [
    CommonModule,
    SolarSystemRoutingModule
  ]
})
export class SolarSystemModule { }
