import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SolarSystem} from '../model/solar-system';

@Injectable({
  providedIn: 'root'
})
export class SolarSystemService {

  constructor(private http: HttpClient) { }

  url = 'https://localhost:5001';

  getSolarSystems(): Observable<Array<SolarSystem>> {
    return this.http.get<[SolarSystem]>(this.url + '/solar-systems');
  }

  newSolarSystem(solarSystem: SolarSystem): Observable<Array<SolarSystem>> {
    return this.http.post<[SolarSystem]>(this.url + '/solar-systems/new', solarSystem);
  }

  editSolarSystem(id: number, solarSystem: SolarSystem): Observable<Array<SolarSystem>> {
    return this.http.post<[SolarSystem]>(this.url + '/solar-systems/' + id + '/edit', solarSystem);
  }

  deleteSolarSystem(solarSystem: SolarSystem): Observable<Array<SolarSystem>> {
    return this.http.delete<[SolarSystem]>(this.url + '/solar-systems/' + solarSystem.id + '/delete');
  }
}
