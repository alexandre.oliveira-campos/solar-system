import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarSystemNewComponent } from './solar-system-new.component';

describe('SolarSystemNewComponent', () => {
  let component: SolarSystemNewComponent;
  let fixture: ComponentFixture<SolarSystemNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarSystemNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarSystemNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
