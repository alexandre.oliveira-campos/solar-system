import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarSystemListComponent } from './solar-system-list.component';

describe('SolarSystemListComponent', () => {
  let component: SolarSystemListComponent;
  let fixture: ComponentFixture<SolarSystemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarSystemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarSystemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
