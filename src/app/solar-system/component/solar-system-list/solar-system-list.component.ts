import { Component, OnInit } from '@angular/core';
import {SolarSystem} from '../../model/solar-system';
import {SolarSystemService} from '../../service/solar-system.service';

@Component({
  selector: 'app-solar-system-list',
  templateUrl: './solar-system-list.component.html',
  styleUrls: ['./solar-system-list.component.scss']
})
export class SolarSystemListComponent implements OnInit {

  constructor(private solarSystemService: SolarSystemService) { }

  solarSystems: SolarSystem[];

  ngOnInit(): void {
    this.getSolarSystems();
  }

  getSolarSystems(): void {
    this.solarSystemService.getSolarSystems().subscribe(
      data =>
      {
        if (data) {
          this.solarSystems = data;
        }
      },
      error => {}
    );
  }

}
