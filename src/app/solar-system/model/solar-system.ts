import {Planet} from '../../planet/model/planet';

export interface SolarSystem {
  id: number;
  name: string;
  planets: Array<Planet>;
}
