import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  login(): void {
    const userInput = document.getElementById('userInput') as HTMLInputElement;
    const user = userInput.value;
    const passwordInput = document.getElementById('passwordInput') as HTMLInputElement;
    const password = passwordInput.value;
    if (user && password && this.authService.checkCredentials(user, password)) {
      this.router.navigate(['/planets']);
    } else {
      const errorMsg = document.getElementById('errorMsg') as HTMLInputElement;
      errorMsg.style.display = '';
    }
  }



}
